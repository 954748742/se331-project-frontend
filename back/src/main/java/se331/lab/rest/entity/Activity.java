package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Activity {
    @Id
    @GenericGenerator(name="kaugen" , strategy="increment")
    @GeneratedValue(generator="kaugen")
    @EqualsAndHashCode.Exclude
    Long id;
    @ManyToOne
    @JsonBackReference
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    Teacher lecturer;
    String desc;
    String loc;
    String name;
    String when;
    String time;
    String regStart;
    String regEnd;
}
