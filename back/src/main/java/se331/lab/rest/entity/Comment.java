package se331.lab.rest.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Comment {
  @Id
  @GenericGenerator(name="kaugen" , strategy="increment")
  @GeneratedValue(generator="kaugen")
  @EqualsAndHashCode.Exclude
  Long id;
  String name;
  String avatar;
  int  activity;
  String msg;
  String imgurl;
  String time;
}
