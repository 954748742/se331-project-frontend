package se331.lab.rest.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.entity.Admin;
import se331.lab.rest.entity.Teacher;
import se331.lab.rest.entity.Person;
import se331.lab.rest.entity.Student;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.User;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    Long id;
    String email;
    String pass;
    boolean enabled;
    String status;
    Date lastPasswordResetDate;
    String type;
    List<Authority> authorities;
  public User getUser() {
    return User.builder()
      .id(this.id)
      .pass(this.pass)
      .status(this.status)
      .email(this.email)
      .type(this.type)
      .enabled(this.enabled)
      .authorities(this.authorities)
      .lastPasswordResetDate(this.lastPasswordResetDate)
      .build();
  }

    public static UserDTO getUserDTO(Person person) {
        if (person instanceof Teacher) {
            Teacher teacher = (Teacher) person;
            return UserDTO.builder()
                    .id(teacher.getId())
                    .pass(teacher.getUser().getPass())
                    .status(teacher.getUser().getStatus())
                    .email(teacher.getUser().getEmail())
                    .type(teacher.getUser().getType())
                    .enabled(teacher.getUser().getEnabled())
                    .authorities(teacher.getUser().getAuthorities())
                    .lastPasswordResetDate(teacher.getUser().getLastPasswordResetDate())
                    .build();
        } else if (person instanceof Student) {
            Student student = (Student) person;
            return UserDTO.builder()
                    .id(student.getId())
                    .pass(student.getUser().getPass())
                    .status(student.getUser().getStatus())
                    .email(student.getUser().getEmail())
                    .type(student.getUser().getType())
                    .enabled(student.getUser().getEnabled())
                    .authorities(student.getUser().getAuthorities())
                    .lastPasswordResetDate(student.getUser().getLastPasswordResetDate())
                    .build();
        }else if (person instanceof Admin) {
            Admin admin = (Admin) person;
          return UserDTO.builder()
            .id(admin.getId())
            .pass(admin.getUser().getPass())
            .status(admin.getUser().getStatus())
            .email(admin.getUser().getEmail())
            .type(admin.getUser().getType())
            .enabled(admin.getUser().getEnabled())
            .authorities(admin.getUser().getAuthorities())
            .lastPasswordResetDate(admin.getUser().getLastPasswordResetDate())
            .build();
        }
        return null;
    }

}
