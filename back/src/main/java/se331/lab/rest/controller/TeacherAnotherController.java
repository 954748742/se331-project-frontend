package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.service.TeacherAnotherService;

@Controller
@Slf4j
public class TeacherAnotherController {
    @Autowired
    TeacherAnotherService teacherAnotherService;
  @CrossOrigin
    @GetMapping("/teacherSurname/{lastname}")
    public ResponseEntity getTeacherBySurname(@PathVariable String lastname) {
        log.info("the controller is call");
        return ResponseEntity.ok(this.teacherAnotherService.getTeacherByLastName(lastname));
    }

}
