package se331.lab.rest.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Activity;
import se331.lab.rest.repository.ActivityRepository;

import java.util.List;
@Repository
public class ActivityAnotherDaoImpl implements ActivityAnotherDao {
    @Autowired
    ActivityRepository activityRepository;
    @Override
    public List<Activity> findAll() {
        return activityRepository.findAll();
    }

    @Override
    public Activity save(Activity activity) {
        return activityRepository.save(activity);
    }

}
