package se331.lab.rest.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Teacher;
import se331.lab.rest.repository.TeacherRepository;

import java.util.List;
@Repository
public class TeacherDaoImpl implements TeacherDao {
    @Autowired
    TeacherRepository teacherRepository;
    @Override
    public List<Teacher> getTeachers() {
        return this.teacherRepository.findAll();
    }

  @Override
  public Teacher saveTeacher(Teacher teacher) {
    return this.teacherRepository.save(teacher);
  }

  @Override
  public Teacher findById(Long id) {
    return teacherRepository.findById(id).orElse(null);
  }
}
