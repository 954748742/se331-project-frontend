package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.TeacherAnotherDao;
import se331.lab.rest.entity.Teacher;

@Service
public class TeacherAnotherServiceImpl implements TeacherAnotherService {
    @Autowired
    TeacherAnotherDao teacherAnotherDao;
    @Override
    public Teacher getTeacherByLastName(String lastname) {
        return teacherAnotherDao.getTeacherByLastName(lastname);
    }
}
