import { TestBed, inject } from '@angular/core/testing';

import { StudentFileImplService } from './student-file-impl.service';

describe('StudentFileImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentFileImplService]
    });
  });

  it('should be created', inject([StudentFileImplService], (service: StudentFileImplService) => {
    expect(service).toBeTruthy();
  }));
});
