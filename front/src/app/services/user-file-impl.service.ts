import { User } from '../entity/user';
import { UserService } from './user.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserFileImplService extends UserService {
    private users = new BehaviorSubject<User[]>(null);

    constructor(private http: HttpClient) {
        super();
        this.http.get<User[]>('assets/users.json').subscribe(users => {
            this.users.next(users);
        });
    }

    load() { }

    obs(): Observable<User[]> {
        return this.users.asObservable();
    }

    get(id: number): User {
        if (this.users == null) return null;
        let list = this.users.getValue();
        return list.find((obj) => obj.id === id);
    }

    private nextId(): number {
        let list = this.users.getValue();
        return list.map(obj => obj.id).reduce((a,b) => a > b ? a : b) + 1;
    }

    add(user: User): User {
        if (this.users == null) return null;
        let list = this.users.getValue();
        user.id = this.nextId();
        list.push(user);
        this.users.next(list);
        return user;
    }

    del(id: number): User {
        if (this.users == null) return null;
        let list = this.users.getValue();
        let user = list.find((obj) => obj.id === id);
        if (user === null) return null;
        let i = list.indexOf(user);
        list.splice(i, 1);
        this.users.next(list);
        return user;
    }

    upd(user: User): User {
        if (this.users == null) return null;
        let list = this.users.getValue();
        let old = list.find((obj) => obj.id === user.id);
        if (old === null) return null;
        let i = list.indexOf(old);
        list[i] = user;
        this.users.next(list);
        return old;
    }

}
