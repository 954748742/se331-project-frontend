import { TestBed, inject } from '@angular/core/testing';

import { StudentApiImplService } from './student-api-impl.service';

describe('StudentApiImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentApiImplService]
    });
  });

  it('should be created', inject([StudentApiImplService], (service: StudentApiImplService) => {
    expect(service).toBeTruthy();
  }));
});
