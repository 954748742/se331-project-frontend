import { Observable } from 'rxjs';
import { Teacher } from '../entity/teacher';

export abstract class TeacherService {
    abstract load();
    abstract obs(): Observable<Teacher[]>;
    abstract get(id: number): Teacher;
    abstract add(teacher: Teacher): Teacher;
    abstract del(id: number): Teacher;
    abstract upd(teacher: Teacher): Teacher;
}
