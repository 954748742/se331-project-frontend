import { Observable } from 'rxjs';
import { Student } from '../entity/student';

export abstract class StudentService {
    abstract load();
    abstract obs(): Observable<Student[]>;
    abstract get(id: number): Student;
    abstract add(student: Student): Student;
    abstract del(id: number): Student;
    abstract upd(student: Student): Student;
}

