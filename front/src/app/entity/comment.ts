export class Comment {
    id: number;
    name: string;
    avatar: string;
    activity: number;
    msg: string;
    imgurl: string;
    time: string;
}
